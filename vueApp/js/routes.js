import VueRouter from 'vue-router';

var routes = [
    { path: '*', redirect: '/' },
    {
        path: '/',
        component: require('./components/Dashboard'),
        meta: {

        }
    },
    // {
    //     path: '/socialSprint',
    //     component: require('./components/socialSprint'),
    //     meta: {
    //
    //     }
    // },
    // {
    //     path: '/manualScheduling',
    //     component: require('./components/manualScheduling'),
    //     meta: {
    //
    //     }
    // },
    // {
    //     path: '/autoPilot',
    //     component: require('./components/autoPilot'),
    //     meta: {
    //
    //     }
    // },
    {
        path: '/socialSettings',
        component: require('./components/socialSettings'),
        meta: {

        }
    },
    // {
    //     path: '/singlePost',
    //     component: require('./components/singlePost'),
    //     meta: {
    //
    //     }
    // },
    // {
    //     path: '/schedulePosts',
    //     component: require('./components/schedulePosts'),
    //     meta: {
    //
    //     }
    // },
    // {
    //     path: '/generalSettings',
    //     component: require('./components/genSettings'),
    //     meta: {
    //
    //     }
    // },
    {
        path: '/audience-list',
        component: require('./components/audienceList'),
        meta: {

        }
    },
    {
        path: '/ca-builder',
        component: require('./components/caBuilder'),
        meta: {

        }
    },
    {
        path: '/laa-builder',
        component: require('./components/laaBuilder'),
        meta: {

        }
    },
    // {
    //     path: '/eventStats',
    //     component: require('./components/eventStat'),
    //     meta: {
    //
    //     }
    // },
    {
        path: '/manageCatalog',
        component: require('./components/manageCatalog'),
        meta: {

        }
    },
    {
        path: '/remarketingRules',
        component: require('./components/remarketingRules'),
        meta: {

        }
    },
    // {
    //     path: '/advancedSettings',
    //     component: require('./components/advancedSettings'),
    //     meta: {
    //
    //     }
    // },
    {
        path: '/createRule',
        component: require('./components/createRule'),
        meta: {

        }
    },
    {
        path: '/manageRules',
        component: require('./components/manageRules'),
        meta: {

        }
    },
    {
        path: '/createCampaign',
        component: require('./components/createCampaign'),
        meta: {

        }
    },
];

export default new VueRouter({
    routes: routes
});